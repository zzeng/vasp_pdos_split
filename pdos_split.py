#!/usr/bin/env python
""" Generate split DOS, integral DOS and band center, band width from DOSCAR
    fermi level is adjusted to 0 eV
    For band center, just the states lower Fermi level are taken into account (it could be adjusted through E_max )
    numpy is required
    Zhenhua Zeng: zeng46@purdue.edu"""
import numpy as np
import os

f=open("DOSCAR")
lines= f.readlines()
f.close()

E_max = 0  # max eigen value used in the band center and band width analysis
print 'max energy (eigen value) in band-center calculation:',E_max, 'eV'

N_lines=len(lines)

NIONS = int(lines[0].split()[0])

E_MAX = float(lines[5].split()[0])
E_MIN = float(lines[5].split()[1])
NEDOS = int(lines[5].split()[2])
E_Fermi= float(lines[5].split()[3])
E_step = (E_MAX-E_MIN)/(NEDOS-1)

if N_lines == (NEDOS+1)*NIONS+5:  #check DOS from relaxtion (just total DOS) or static calculation
	F_relax = 0
else:
	F_relax = 1

f_lorbit=os.popen('grep "LORBIT" OUTCAR | tail -1 ','r')
L_lorbit = f_lorbit.readline()
LORBIT = int(L_lorbit.split()[2])

f_ispin=os.popen('grep "ISPIN" OUTCAR | tail -1 ','r')
L_ispin = f_ispin.readline()
ISPIN = int(L_ispin.split()[2])


print ' ISPIN = %d'%ISPIN
print 'LORBIT = %d'%LORBIT
print ''
if F_relax == 0:
	print 'DOS generated during relaxing'
print ""
print 'Fermi level= %f eV'%E_Fermi
print ""
DOS_ALL_T = np.zeros(NEDOS)               # DOS for ALL atoms
E_x = np.zeros(NEDOS)                     # energy axis
DOS_Atom_T = np.zeros((NIONS,NEDOS))        # total DOS for atom i 
DOS_Atom_Pl = np.zeros((NIONS,NEDOS,3))     # l projected DOS for atom i

for i in range(NEDOS):
    fline=[np.float(s) for s in lines[6+i].split()]  # line in float format
    epsilon = fline[0]-E_Fermi   # adjust Fermi Level to 0 eV
    if epsilon <= E_max:
	E_x[i] = epsilon
    else:
	E_x[i] = 0
    DOS_ALL_T[i] = fline[1]

if ISPIN == 1:
    if LORBIT%5 == 0:
	DOS_Atom_Plm = np.zeros((NIONS,NEDOS,3))      # lm projected DOS for atom i
	DOS_Atom_int = np.zeros((NIONS,3))
	for i in range(NIONS):
	    for j in range(NEDOS):
		line=lines[6+(NEDOS+1)*(i+F_relax)+j]
                fline=[np.float(s) for s in line.split()]
		DOS_Atom_Plm[i][j] = fline[1:4]
		DOS_Atom_Pl[i][j] = DOS_Atom_Plm[i][j]
		DOS_Atom_T[i][j] = DOS_Atom_Plm[i][j].sum()
    elif LORBIT%10 == 1 or LORBIT%10 == 2:
	DOS_Atom_Plm = np.zeros((NIONS,NEDOS,9))      # lm projected DOS for atom i
        DOS_Atom_int = np.zeros((NIONS,9))
        for i in range(NIONS):
            for j in range(NEDOS):
		fline=[np.float(s) for s in lines[6+i].split()]
                fline=[np.float(s) for s in line.split()]
                DOS_Atom_Plm[i][j] = fline[1:]
		DOS_Atom_Pl[i][j][0] = DOS_Atom_Plm[i][j][0]
		DOS_Atom_Pl[i][j][1] = DOS_Atom_Plm[i][j][1:4].sum()
                DOS_Atom_Pl[i][j][2] = DOS_Atom_Plm[i][j][4:9].sum()
                DOS_Atom_T[i][j] = DOS_Atom_Plm[i][j].sum()
elif ISPIN == 2:
    if LORBIT%5 == 0:
        DOS_Atom_Plm = np.zeros((NIONS,NEDOS,6),Float)      # lm projected DOS for atom i
        DOS_Atom_int = np.zeros((NIONS,6),Float)
        for i in range(NIONS):
            for j in range(NEDOS):
                fline=[np.float(s) for s in lines[6+i].split()]
                fline=[np.float(s) for s in line.split()]
                DOS_Atom_Plm[i][j] = fline[1:]
                DOS_Atom_Pl[i][j][0] = DOS_Atom_Plm[i][j][0:2].sum()
                DOS_Atom_Pl[i][j][1] = DOS_Atom_Plm[i][j][2:4].sum()
                DOS_Atom_Pl[i][j][2] = DOS_Atom_Plm[i][j][4:6].sum()
                DOS_Atom_T[i][j] = DOS_Atom_Plm[i][j].sum()
    elif LORBIT%10 == 1 or LORBIT%10 == 2:
        DOS_Atom_Plm = np.zeros((NIONS,NEDOS,18),Float)      # lm projected DOS for atom i
        DOS_Atom_int = np.zeros((NIONS,18),Float)
        for i in range(NIONS):
            for j in range(NEDOS):
                fline=[np.float(s) for s in lines[6+i].split()]
                fline=[np.float(s) for s in line.split()]
                DOS_Atom_Plm[i][j] = fline[1:]

                DOS_Atom_Pl[i][j][0] = DOS_Atom_Plm[i][j][0:2].sum()
                DOS_Atom_Pl[i][j][1] = DOS_Atom_Plm[i][j][2:8].sum()
                DOS_Atom_Pl[i][j][2] = DOS_Atom_Plm[i][j][8:18].sum()
                DOS_Atom_T[i][j] = DOS_Atom_Plm[i][j].sum()

def integrated_PDOS(PDOS):
    N_PDOS = len(PDOS[0][0])
    DOS_Atom_int = np.zeros((NIONS,N_PDOS))
    xdata = np.array(E_x)
    for i in range(NIONS):
	for j in range(N_PDOS):
	    DOS_Atom_int[i][j]=np.trapz(PDOS[i][:,j],xdata)
    return DOS_Atom_int
#	print DOS_Atom_int[i]
 
def Moment(xdata,ydata,nth=1):
                """
                Internal function to calculate the nth moment  of the
                xdata,ydata distribution, by default the first moment
                (center of energy) 
                """
                xdata=np.array(xdata)
                ydata=np.array(ydata)
 
                data=(xdata**(nth))*ydata
                #print data  # zengzh
                normalization=np.trapz(ydata,xdata)
		#print normalization
 
                if nth == 0:
                        return normalization
                elif normalization > 1.0e-6:
                        return np.trapz(data,xdata)/normalization
		else:
			return 0

def band_center(PDOS):
    N_PDOS = len(PDOS[0][0])
    center_Band = np.zeros((NIONS,N_PDOS))
    xdata = np.array(E_x)
    for i in range(NIONS):
	for j in range(N_PDOS):
	    center_Band[i][j] = Moment(xdata,PDOS[i][:,j])
    return center_Band

def band_width(PDOS):
    N_PDOS = len(PDOS[0][0])
    width_Band = np.zeros((NIONS,N_PDOS))
    xdata = np.array(E_x)
    for i in range(NIONS):
        for j in range(N_PDOS):
            width_Band[i][j] = Moment(xdata,PDOS[i][:,j], 2)**0.5  # band width is the squre root of the second moment(PRB 82,045414,2010) 
    return width_Band

    
DOS_Atom_int = integrated_PDOS(DOS_Atom_Plm)
Band_Cent_l  = band_center(DOS_Atom_Pl)
Band_Cent_lm = band_center(DOS_Atom_Plm)
Band_Widt_l  = band_width(DOS_Atom_Pl)
Band_Widt_lm = band_width(DOS_Atom_Plm)

if not os.path.isdir('PDOS'):
	os.system('mkdir PDOS')
f_c_l  = './PDOS/band_cent_l.dat'
f_c_lm = './PDOS/band_cent_lm.dat'
f_w_l  = './PDOS/band_width_l.dat'
f_w_lm = './PDOS/band_width_lm.dat'
c_l  = open(f_c_l,'w')
c_lm = open(f_c_lm,'w')
w_l  = open(f_w_l,'w')
w_lm = open(f_w_lm,'w')

print >> c_l,'%-6s'%('Atom'),3*'%-11s'%('s_cent','p_cent','d_cent')
print >> w_l,'%-5s'%('Atom'),3*'%-11s'%('s_width','p_width','d_width')

for i in range(1):  # write file head
    if ISPIN==1:
        if LORBIT%5 == 0:
            print >>c_lm,'%-6s'%('Atom'),3*'%-11s'%('s_cent','p_cent','d_cent')
            print >>w_lm,'%-5s'%('Atom'),3*'%-11s'%('s_width','p_width','d_width')
        elif LORBIT%10 ==1 or LORBIT%10 == 2:
            print >>c_lm,'%-6s'%('Atom'),9*'%-11s'%('s_cent','py_cent','pz_cent','px_cent',\
				'dxy_cent','dyz_cent','dz2_cent','dxz_cent','dx2-y2_cent' )
            print >>w_lm,'%-5s'%('Atom'),9*'%-11s'%('s_width','py_width','pz_width','px_width',\
                                'dxy_width','dyz_width','dz2_width','dxz_width','dx2-y2_width' )
    elif ISPIN==2:
        if LORBIT%5 == 0:
            print >>c_lm,'%-6s'%('Atom'),6*'%-11s'%('s_up_cent','s_dn_cent','p_up_cent',\
						'p_dn_cent','d_up_cent','d_dn_cent')
            print >>w_lm,'%-5s'%('Atom'),6*'%-11s'%('s_up_width','s_dn_width','p_up_width',\
                                                'p_dn_width','d_up_width','d_dn_width')
        elif LORBIT%10 ==1 or LORBIT%10 == 2:
            print >>c_lm,'%-6s'%('Atom'),18*'%-11s'%('s_up_cent','s_dn_cent','py_up_cent','py_dn_cent','pz_up_cent',\
			'pz_dn_cent','px_up_cent','px_dn_cent','dxy_up_cent','dxy_dn_cent','dyz_up_cent',\
			'dyz_dn_cent','dz2_up_cent','dz2_dn_cent','dxz_up_cent','dxz_dn_cent',\
			'dx2-y2_up_cent','dx2-y2_dn_cent')
            print >>w_lm,'%-5s'%('Atom'),18*'%-11s'%('s_up_width','s_dn_width','py_up_width','py_dn_width','pz_up_width',\
                        'pz_dn_width','px_up_width','px_dn_width','dxy_up_width','dxy_dn_width','dyz_up_width',\
                        'dyz_dn_width','dz2_up_width','dz2_dn_width','dxz_up_width','dxz_dn_width',\
                        'dx2-y2_up_width','dx2-y2_dn_width')

for i in range(NIONS):
    print >> c_l,'%-4d %10.6f %10.6f %10.6f'%(i+1,Band_Cent_l[i][0],Band_Cent_l[i][1],Band_Cent_l[i][2])

    print >> c_lm,'%-4d'%(i+1),
    for j in range(len(Band_Cent_lm[0])):
	print >> c_lm,'%10.6f'%Band_Cent_lm[i][j],
    print >> c_lm,''

    print >> w_l,'%-4d %10.6f %10.6f %10.6f'%(i+1,Band_Widt_l[i][0],Band_Widt_l[i][1],Band_Widt_l[i][2])

    print >> w_lm,'%-4d'%(i+1),
    for j in range(len(Band_Widt_lm[0])):
        print >> w_lm,'%10.6f'%Band_Widt_lm[i][j],
    print >> w_lm,''


c_l.close()
c_lm.close()
w_l.close()
w_lm.close()


for i in range(NIONS):
    f_tmp = './PDOS/PDOS_atom%d.dat'%(i+1)
    f_dos=open(f_tmp,'w')
    #-----------1st line-------------#
    if ISPIN==1:
	if  LORBIT%5 == 0 :
	    print >>f_dos,'%-10s'%('States'),3*'%-10s'%('s','p','d')
	elif LORBIT%10 ==1 or LORBIT%10 == 2:
            print >>f_dos,'%-10s'%('States'),9*'%-10s'%('s','py','pz','px','dxy','dyz','dz2','dxz','dx2-y2' )
    elif ISPIN==2:
        if LORBIT%5 == 0:
            print >>f_dos,'%-10s'%('States'),6*'%-10s'%('s_up','s_dn','p_up','p_dn','d_up','d_dn')
        elif LORBIT%10 ==1 or LORBIT%10 == 2:
            print >>f_dos,'%-10s'%('States'),18*'%-10s'%('s_up','s_dn','py_up','py_dn','pz_up','pz_dn','px_up','px_dn',\
			'dxy_up','dxy_dn','dyz_up','dyz_dn','dz2_up','dz2_dn','dxz_up','dxz_dn','dx2-y2_up','dx2-y2_dn')
    #-----------2nd line-------------#
    print >>f_dos,'%-10s'%('DOS_int'),
    for k in range(len(DOS_Atom_int[i])):
	print >>f_dos,'%-9.2f'%(DOS_Atom_int[i][k]),
    print >>f_dos,''
    #-----------DOS data-------------#
    for j in range(NEDOS):
	print >>f_dos,'%10.6f'%(E_step*j+E_MIN-E_Fermi), # adjust Fermi Level to 0 eV
	for k in range(len(DOS_Atom_Plm[i][j])):
	    print >>f_dos,'%6.3e'%DOS_Atom_Plm[i][j][k],
	print >>f_dos,''

